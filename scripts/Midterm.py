'''
Trey Hewell
Midterm1.py
10/13/23
'''

#!/usr/bin/env python

from __future__ import print_function
from six.moves import input

# to use Python MoveIt interfaces, import moveit_commander namespace
# This namespace provides the MoveGroupCommander, PlanningSceneInterface, and RobotCommander classes
# import rospy

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import numpy as np

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class Midterm(object): # Create class to define all functions within
    def __init__(self): # initialize nodes and objects to communicate between ROS, Rviz, and Gazebo 
        super(Midterm, self).__init__()

        # initialize moveit_commander and rospy node
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        # Create a RobotCommander object that provides information about the robot's kinematic model
        # and current joint states
        robot = moveit_commander.RobotCommander()

        # Create a PlanningSceneInterface object that provides a remote interface for getting,
        # setting, and updating the robot's internal understanding of the world
        scene = moveit_commander.PlanningSceneInterface()

        # Create a MoveGroupCommander object to interface with a group of joints
        # This will be specific to URD5 robot arm
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        # Create a DisplayTrajectory ROS publisher to display trajectories in Rviz
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )
        # Print name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        # Print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        # Print a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Available Planning Groups:", robot.get_group_names())

        # Print the entire state of the robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        # Other variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self): # Joint goal function used to move robot to initial position
       
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = np.pi / 2
        joint_goal[1] = np.pi / -4.74
        joint_goal[2] = np.pi / 2.28
        joint_goal[3] = np.pi / -0.80
        joint_goal[4] = np.pi / -1.73
        joint_goal[5] = np.pi / -0.66

        # The go command can be called with joint values, poses, or without any
        # parameters if you have already set the pose or joint target for the group
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

    def plan_cartesian_path_T(self, scale=1): # Cartesian path function for robot to follow to draw "T"
    
        move_group = self.move_group

        

        waypoints = [] # Create empty list of waypoints for robot to pass through

        wpose = move_group.get_current_pose().pose
        wpose.position.z = scale * 0.5  # First move up (z)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x = scale * -0.2  # Second move right in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x = scale * 0.2  # Third move left in (x)
        waypoints.append(copy.deepcopy(wpose))

        # 0.01 is the eef_step in Cartesian translation.  We will disable the
        # jump threshold by setting it to 0.0,
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction
    
    def plan_cartesian_path_C(self, scale=1): # Cartesian path function for robot to draw "C"
    
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        wpose.position.z = scale * 0.5 + 0.18871800689423654 # Move to initial drawing point, up in (z)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.475 +0.18871800689423654 # Down in (z)
        wpose.position.x = scale * 0.2 + -0.08929677343051583 # Left in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.4 + 0.18871800689423654  # Down in (z)
        wpose.position.x = scale * 0.3 + -0.08929677343051583 # Down in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.25 + 0.18871800689423654 # Down in (z)
        wpose.position.x = scale * 0.35 + -0.08929677343051583 # Down in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.1 + 0.18871800689423654 # Down in (z)
        wpose.position.x = scale * 0.3 + -0.08929677343051583  # Right in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.025 + 0.18871800689423654 # Down in (z)
        wpose.position.x = scale * 0.2 + -0.08929677343051583 # Right in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.18871800689423654 # Down in (z)
        wpose.position.x = scale * -0.08929677343051583 # Right in (x)
        waypoints.append(copy.deepcopy(wpose))
      
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  
        )

        return plan, fraction
    
    def plan_cartesian_path_H(self, scale=1): # Cartesian path function for drawing H
    
        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        wpose.position.x = scale * 0.1  # Move to initial drawing point, left in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.5 # Draw up in (z)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.25 # and back down halfway (z)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x = scale * -0.2 # Move right in (x)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0.5 # Move back up in (z)
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z = scale * 0 # Move back to base (z)
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  
        ) 

        return plan, fraction
    
    def execute_plan(self, plan):

        move_group = self.move_group

        move_group.execute(plan, wait=True)

def main(): # Executing function to run all code
    try:
        print("")
        print("----------------------------------------------------------")
        print("Welcome to Trey Hewell's Robotics Midterm!")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")
        input(
            "============ Press `Enter` to begin the simulation"
        )
        tutorial = Midterm()

        input(
            "============ Press `Enter` to arrive at the starting point using joint state goal"
        )
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to draw a T")
        cartesian_plan, fraction = tutorial.plan_cartesian_path_T()
        tutorial.execute_plan(cartesian_plan)

        input(
            "============ Press `Enter` to return to the starting point using joint state goal"
        )
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to draw a C")
        cartesian_plan, fraction = tutorial.plan_cartesian_path_C()
        tutorial.execute_plan(cartesian_plan)

        input(
            "============ Press `Enter` to return to the starting point using joint state goal"
        )
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to draw an H")
        cartesian_plan, fraction = tutorial.plan_cartesian_path_H()
        tutorial.execute_plan(cartesian_plan)

        input(
            "============ Press `Enter` to return to the final resting position using joint state"
        )
        tutorial.go_to_joint_state()

        print("============ Midterm 1 Complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()