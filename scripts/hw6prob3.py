from __future__ import print_function
from six.moves import input

# to use Python MoveIt interfaces, import moveit_commander namespace
# This namespace provides the MoveGroupCommander, PlanningSceneInterface, and RobotCommander classes
# import rospy

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))

# import other messages to be used

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# initialize moveit_commander and rospy node

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

# Instantiate a RobotCommander object that provides information about the robot's kinematic model
# and current joint states

robot = moveit_commander.RobotCommander()

# Instantiate a PlanningSceneInterface object that provides a remote interface for getting,
# setting, and updating the robot's internal understanding of the world

scene = moveit_commander.PlanningSceneInterface()

# Instantiate a MoveGroupCommander object to interface with a group of joints
# This will be for URD5 robot arm

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Create a DisplayTrajectory ROS publisher to display trajectories in Rviz

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# Name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# Print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# Get a list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Print the entire state of the
# robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")

# We get the joint values from the group and change some of the values:
# Plan Cartesian path by setting waypoints for the end effector
#!/usr/bin/env python

import numpy as np

# ... [your initialization code here]

# Define the starting point and dimensions for the "T" shape
start_x = 0.3
start_y = 0.3
height = 0.4
width = 0.3
elevated_z = 0.1  # Elevate the trajectory to avoid singularities

# Generate waypoints
waypoints = []
scale = 1.0

# Vertical segment of the "T"
for i in range(11):  # Creating 10 waypoints for smoothness
    x = start_x + width/2  # Center of the "T"
    y = start_y + i * (height / 10.0)
    pose = geometry_msgs.msg.Pose()
    pose.position.x = x
    pose.position.y = y
    pose.position.z = elevated_z
    pose.orientation.x = 0
    pose.orientation.y = 0
    pose.orientation.z = 0
    pose.orientation.w = 1.0
    waypoints.append(pose)

# Horizontal segment of the "T"
for i in range(11):  # Creating 10 waypoints for smoothness
    x = start_x + i * (width / 10.0)
    y = start_y + height  # Top of the vertical segment
    pose = geometry_msgs.msg.Pose()
    pose.position.x = x
    pose.position.y = y
    pose.position.z = elevated_z
    pose.orientation.x = 0
    pose.orientation.y = 0
    pose.orientation.z = 0
    pose.orientation.w = 1.0
    waypoints.append(pose)

# Plan and execute in smaller segments
segment_length = 50  # Adjust as needed
for i in range(0, len(waypoints), segment_length):
    segment = waypoints[i:i+segment_length]
    (plan, fraction) = move_group.compute_cartesian_path(segment, 0.01, 0.0)
    
    if fraction < 1.0:
        print("Warning: Not all waypoints in this segment were feasible!")
    
move_group.execute(plan, wait=True)
