#!/usr/bin/bash

rosservice call /turtle1/set_pen "255" "0" "0" "5" "0"
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[-4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -5.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /spawn 7 5.5 0 ""

rosservice call /turtle2/set_pen "0" "255" "0" "5" "0"
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -5.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 4.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[4.5, 4, 0.0]' '[0.0, 0.0, -4.5]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -2.5]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[3.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'